# Maintainer: Antoni Aloy <aaloytorrens@gmail.com>
pkgname=py3-imageio
pkgver=2.23.0
pkgrel=0
pkgdesc="Python library that provides an easy interface to read and write a wide range of image data"
url="https://github.com/imageio/imageio"
license="BSD-2-Clause"
# ppc64le: test failures
# s390x: freeimage
arch="noarch !ppc64le !s390x"
depends="python3 py3-numpy py3-pillow freeimage"
makedepends="py3-setuptools"
checkdepends="py3-pytest py3-psutil py3-imageio-ffmpeg py3-fsspec"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/i/imageio/imageio-$pkgver.tar.gz"
builddir="$srcdir/imageio-$pkgver"
options="!check" # intentionally fail without internet(?), todo

build() {
	python3 setup.py build
}

check() {
	PYTHONPATH="$PWD"/build/lib IMAGEIO_NO_INTERNET=1 pytest -v
}

package() {
	python3 setup.py install --root="$pkgdir" --optimize=1

	# remove unneeded binaries
	rm -r "$pkgdir"/usr/bin
}

sha512sums="
ca27086c31fcef9da2e693de0e7f8dda1ba82b692eaed742013babb8f837b4028b3332eeb3772b573bd892c24cd9139135b7f9884c63c35a97d3c82717110cff  py3-imageio-2.23.0.tar.gz
"
